if grub-file --is-x86-multiboot $1
then
  echo "Multiboot check             $(tput setaf 2)[OK]$(tput sgr0)"
  echo $1
  bash create_iso.sh $1
else
  echo "Multiboot check             $(tput setaf 1)[ERROR]$(tput sgr0)"
  echo "Errors detected, exiting build process"
fi
