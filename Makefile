SRC :=src 
BUILD :=build
ASM=nasm
GCC = i686-elf-gcc
LD = i686-elf-ld 
GCC_PARAMS = -ffreestanding -O2 -Wall -Wextra
LD_PARAMS = -ffreestanding -O2 -nostdlib

OBJECTS = src/kernel/kernel.o src/boot/boot.o src/drivers/screen/screen.o src/utils/string/string.o src/cpu/gdt/gdt_entry.o src/cpu/gdt/gdt.o src/cpu/descriptors.o src/cpu/idt/idt_entry.o src/cpu/idt/idt.o src/cpu/interrupts/interrupts.o src/cpu/interrupts/isr.o 

%.o:%.asm
	nasm -f elf32 $< -o $@

%.o: %.c
	$(GCC) $(GCC_PARAMS) -c -g $< -o $@

$(BUILD)/terminalos.elf: linker.ld $(OBJECTS)
	$(GCC) -T linker.ld -o $@ $(LD_PARAMS) $(OBJECTS)

terminalos.iso: $(BUILD)/terminalos.elf
	bash prebootchecks.sh $<

run: terminalos.iso
	qemu-system-i386 -cdrom $<

debug: terminalos.iso
	bash debug.sh

clean:
	find . -name "*.o" -type f -delete
	find . -name "*.bin" -type f -delete
	find . -name "*.cfg" -type f -delete
	find . -name "*.out" -type f -delete
	find . -name "*.elf" -type f -delete

