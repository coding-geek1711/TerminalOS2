#include <stdio.h>

int main()
{
    printf("size of int is %lu\n", sizeof(int));
    printf("size of char is %lu\n", sizeof(char));
    printf("Size of short is %lu\n", sizeof(short));
    printf("Size of long is %lu\n", sizeof(long));
    return 0;
}