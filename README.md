# Terminal OS

## Author: Maheswaran
## Date: 20th Nov 2021

## PROGRESS

- [x] Multiboot compilance achieved                                                             
- [x] VGA driver from complete with print_hex, print_dec, printf functions, No color though
- [x] Setup gdb based debugging (A bit hacky makefile method, but still works)
- [x] Shift to 32 bit mode soon, need to write GDT and IDT codes in C (GDT done)
- [ ] Making a FAT12 filesystem for the same, the most basic of filesystems to exist

## Installation
- Setup a 32 bit cross compiler by following the instructions given [here](https://wiki.osdev.org/GCC_Cross-Compiler)
- Setup NASM, Assembler for compiling x86 programs
- Run the make file by make run

## Debugging
- Ensure u make run the file once
- Then make debug the file, without cleaning anything
- A qemu shell with open 1234 localhost port is created
- On new shell, gdb and target remote localhost:1234 to connect to qemu
- Load the kernel image symbols by symbol-file path_to_kernel_symbol (debug/terminalos.elf)
- b kernel_main

## Abstract:

- Hobbyist approach towards creating a Toy OS, capable of handling data with some simple filesystem like FAT12
- GRUB as the bootloader, multiboot structure is finished
- More insight on Segmentation needed, like what exactly happens with the GDT and IDT (have some idea but not quite sure)
- Exactly how is a filesystem, however simple, made. (BIG ISSUE as no tuts found anywhere on the internet)
- Floppy/Hard disk driver implementation

## References:
- osdev.wiki
- james malloy implementation sites