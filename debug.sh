# Compile with debug symbols here

# gnome-terminal -- gdb -e"



gnome-terminal -- gdb -ex "target remote localhost:1234" -ex "symbol-file build/terminalos.elf" -ex "layout asm" -ex "b isr_common_stub"
if test -f "build/terminalos.elf"
then
	qemu-system-i386 -s -S terminalos.iso
else
    cp $(BUILD)/terminalos.elf debug/terminalos.elf
    objcopy --only-keep-debug debug/terminalos.elf debug/terminalos.sym 
    objcopy --strip-debug debug/terminalos.elf
    qemu-system-i386 -s -S terminalos.iso
fi	
