#ifndef __GDT__
#define __GDT__

#include "../../utils/string/string.h"
#include "../../utils/types.h"


struct GlobalDescriptorTableEntry
{
    uint16_t limit_low;
    uint16_t base_low;
    uint8_t base_middle;
    uint8_t access;
    uint8_t granularity;
    uint8_t base_high;
}__attribute__((packed));

struct GlobalDescriptorTablePointer
{
    uint16_t limit;
    uint32_t base;
}__attribute__((packed));

typedef struct GlobalDescriptorTableEntry gdt_entry_t;
typedef struct GlobalDescriptorTablePointer gdt_ptr_t;

void init_gdt();

#endif