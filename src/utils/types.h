#ifndef __TYPES__
#define __TYPES__

typedef unsigned char   uint8_t;
typedef          char   sint8_t;
typedef unsigned short  uint16_t;
typedef          short  sint16_t;
typedef unsigned int    uint32_t;
typedef          int    sint32_t; 

#endif 