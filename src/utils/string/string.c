#include "string.h"

void print_hex(int val)
{
    int len = intlen(val);

    char string[len + 2];
    len += 1;

    string[len] = '\0';
    string[0] = '0';
    string[1] = 'x';

    len--;

    while(val != 0)
    {
        int remainder = val % 16;
        if(remainder > 9)
        {
            string[len] = '0' + 7 + val % 16;
        } else {
            string[len] = '0' + val % 16;
        }
        val = val / 16;
        len--;
    }
    printf(string);
}

void print_dec(int val)
{
    int len = intlen(val);
    char string[len + 1];

    string[len] = '\0';
    len--;

    while(val != 0)
    {
        string[len] = '0' + val % 10;
        val = val / 10;
        len--;
    }
    
    printf(string);

}

int strlen(uint8_t* string)
{
    int count = 0;
    int i = 0;
    while(string[i] != '\0')
        count++;
    return count;
}

int intlen(int val)
{
    int count = 0;
    while(val != 0)
    {
        val = val / 10;
        count++;
    }
    return count;
}

int strcmp(char* str1, char* str2)
{
    int len1 = strlen(str1);
    int len2 = strlen(str2);

    if(len1 != len2) return 1;

    for(int i = 0; i < len1; i++)
    {
        if(str1[i] != str2[i])
            return 1;
    }
    return 0;
}