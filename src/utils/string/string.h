#ifndef __STRING__
#define __STRING__

#include "../../drivers/screen/screen.h"
#include "../types.h"

void print_hex(int val);
void print_dec(int val);
int strlen(uint8_t* data);

#endif