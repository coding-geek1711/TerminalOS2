#include "screen.h"

int rows = 0, cols = 0;
uint8_t* VID_ADD = (uint8_t*)VIDEO_MEMORY;


void clear_screen(void)
{
    for(int i = 0; i <= 80*25; i++)
        VID_ADD[i*2] = ' ';
}

void printf(uint8_t* string)
{
    for(int i = 0; string[i] != '\0'; i++)
    {
        if(string[i] == '\n')
        {
            rows++;
            cols = 0;
        } else if (rows > 25)
        {
            clear_screen();
            rows = 0;
            cols = 0;
        } else
        { 
            VID_ADD[(rows * 80 + cols)*2] = string[i];
            cols++;
        }
    }
}

void terminal_initialise()
{
    clear_screen();
    printf("Hello World!!!\n");
    printf("Maheswaran here");
}