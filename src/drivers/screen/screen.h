#ifndef __SCREEN__
#define __SCREEN__

#include "../../utils/types.h"

#define VIDEO_MEMORY 0xB8000

void clear_screen(void);
void printf(uint8_t* string);
void terminal_initialise();

#endif