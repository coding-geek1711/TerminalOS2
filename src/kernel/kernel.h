#ifndef __KERNEL__
#define __KERNEL__

#include "../drivers/screen/screen.h"
#include "../utils/string/string.h"
#include "../cpu/descriptors.h"

void kernel_main(void);

#endif