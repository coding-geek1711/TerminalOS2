# Create grub cfg
echo "menuentry \"TerminalOS\" {" >> grub.cfg 
echo "          multiboot /boot/terminalos.elf" >> grub.cfg 
echo "}" >> grub.cfg

mkdir -p isodir/boot/grub
cp $1 isodir/boot/terminalos.elf
cp grub.cfg isodir/boot/grub/grub.cfg 
grub-mkrescue -o terminalos.iso isodir
# rm -rf isodir
# rm -rf grub.cfg